import React, { Component } from 'react';
import format from 'date-fns/format';
import Cell from './Cell';

class WeekColumn extends Component {
    renderBlocks = () => {
        const max = 24;
        const date = new Date();
        date.setMinutes(0);
        const blocks = Array.from({ length: max }, (_, hour) => {
            return (
                <Cell
                    className="row-item day"
                    key={hour}
                    time={format(date.setHours(hour), 'hh:mm A')}
                />
            );
        });
        return blocks;
    };
    render() {
        return (
            <div className="grid">
                <div className="row-item week time extra">
                    <span className="d-block">Time</span>
                </div>
                <div className="grid-row week flex-column">
                    {this.renderBlocks()}
                </div>
            </div>
        );
    }
}

export default WeekColumn;
