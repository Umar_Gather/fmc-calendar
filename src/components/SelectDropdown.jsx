import React from 'react';
import format from 'date-fns/format';

const SelectDropdown = ({ dropdownData, view, handleChange, startWeeks, endWeeks, currentDate }) => {
    const items = dropdownData(view);
    const start = startWeeks();
    const end = endWeeks();
    const dates = [];
    if (view === 'Week') {
        start.forEach((week, i) => {
            dates.push(`${format(week, 'Do')} - ${format(end[i], 'Do MMMM YYYY')}`);
        });
    }
    return (
        <select className="select-dropdown" onChange={handleChange}>
            {view === 'Day' &&
                items.map((item) => (
                    <option time={item} key={item} view={view} >
                        {format(item, 'Do MMMM YYYY')}
                    </option>
                ))
            }
            {view === 'Week' &&
                dates.map((item, i) => (
                    <option time={start[i]} key={item} view={view}>
                        {item}
                    </option>
                ))
            }
            {view === 'Month' &&
                items.map((item, i) => (
                    <option time={i} key={item} view={view}>
                        {format(item, 'MMMM')}
                    </option>
                ))
            }
        </select>
    );
};

export default SelectDropdown;
