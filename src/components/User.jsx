import React, { Component } from 'react';
import { Row, Col } from 'reactstrap';

class User extends Component {
    state = {
        name: 'Pizza Express',
        logo: 'http://via.placeholder.com/350x150',
        cuisine: 'Italian',
        city: 'Manchester',
        postcode: 'M1 20B',
        tel: '+44 (0) 161 237 6879',
        email: 'manager@pizzaexpress-manchestser.co.uk',
    };
    render() {
        return (
            <div className="user-info">
                <Row>
                    <Col
                        sm="12"
                        md="12"
                        lg="2"
                        xl="1"
                        className="align-self-end"
                    >
                        <div className="image-wrapper">
                            <img
                                className="image"
                                src={this.state.logo}
                                alt="company logo"
                            />
                        </div>
                    </Col>
                    <Col sm="12" md="12" lg="10" xl="11">
                        <Row>
                            <Col xs="6">
                                <span className="heading-secondary">
                                    {this.state.name}
                                </span>
                                <span className="cuisine">
                                    {this.state.cuisine}
                                </span>
                                <span className="location mt-2">
                                    {this.state.city +
                                        ' ' +
                                        this.state.postcode}
                                </span>
                            </Col>
                            <Col xs="6" className="align-self-end text-right">
                                <span className="tel">{this.state.tel}</span>
                                <span className="email">
                                    {this.state.email}
                                </span>
                            </Col>
                        </Row>
                    </Col>
                </Row>
            </div>
        );
    }
}

export default User;
