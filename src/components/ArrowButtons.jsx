import React from 'react';

const arrowLeft = (
  <svg viewBox="0 0 6.2 10.2" xmlns="http://www.w3.org/2000/svg">
    <path
      d="M4.7.2A.758.758,0,0,1,5.1,0a.758.758,0,0,1,.4.2L6,.7a.52.52,0,0,1,.2.4c0,.2,0,.3-.2.4L2.4,5.1,6,8.7c.2.2.2.3.2.4a.4.4,0,0,1-.2.4l-.5.5a.5.5,0,0,1-.8,0L.2,5.5a.5.5,0,0,1,0-.8Z"
      fill="#1E2238"
      id="left-arrow"
    />
  </svg>
);

const arrowRight = (
  <svg viewBox="0 0 6.2 10.3" xmlns="http://www.w3.org/2000/svg">
    <path
      d="M9,10.1a.5.5,0,0,1-.8,0l-.5-.6a.52.52,0,0,1-.2-.4c0-.2,0-.3.2-.4l3.6-3.6L7.7,1.5a.758.758,0,0,1-.2-.4A.758.758,0,0,1,7.7.7L8.2.2A.5.5,0,0,1,9,.2l4.5,4.5a.5.5,0,0,1,0,.8Z"
      fill="#1E2238"
      id="right-arrow"
      transform="translate(-7.5)"
    />
  </svg>
);

const ArrowButtons = ({ prev, view , next}) => (
    <ul className="buttons">
      <li className="left arrow" onClick={() => prev(view)}>
        {arrowLeft}
      </li>
      <li className="right arrow" onClick={() => next(view)}>
        {arrowRight}
      </li>
    </ul>
);

export default ArrowButtons;
