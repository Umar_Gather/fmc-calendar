import React, { Fragment } from 'react';
import PropTypes from 'prop-types';
import format from 'date-fns/format';

const GridHeading = ({ weeksWithDates, view, getWeekDays }) => {
    const days = weeksWithDates();
    return (
        <Fragment>
            {view === 'Month' && (
                <div className="grid-row text-center bg-grey">
                    {getWeekDays().map((day) => (
                        <div key={day} className="row-item week">
                            {day}
                        </div>
                    ))}
                </div>
            )}
            {view === 'Week' && (
                <div className="grid-row text-center bg-grey">
                    {days.map((day) => (
                        <div key={day} className="row-item week time extra">
                            <span className="d-block">
                                {format(day, 'ddd')}
                            </span>
                            {format(day, 'DD/MM')}
                        </div>
                    ))}
                </div>
            )}
            {view === 'Day' && null}
        </Fragment>
    );
};

GridHeading.propTypes = {
    view: PropTypes.string.isRequired,
    weeksWithDates: PropTypes.func,
    getWeekDays: PropTypes.func,
};

export default GridHeading;
