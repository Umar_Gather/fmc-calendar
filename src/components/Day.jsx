import React, { Fragment } from 'react';
import Bookings from './Bookings';
import RequestTable from './RequestTable';
import CancelTable from './CancelTable';
import BookingsTable from './BookingsTable';

const Day = () => {
    const request = Bookings.filter((item) => item.status === 'Request');
    const cancel = Bookings.filter((item) => item.status === 'Cancelled');
    const booking = Bookings.filter((item) => item.status === 'Booking');
    return (
        <Fragment>
            {request.length > 0 && <RequestTable items={request} />}
            {cancel.length > 0 && <CancelTable items={cancel} />}
            {booking.length > 0 && <BookingsTable items={booking} />}
        </Fragment>
    );
};

export default Day;
