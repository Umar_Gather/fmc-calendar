import React, { Component } from 'react';
import { Container } from 'reactstrap';
import User from './User'; // eslint-disable-line
import TopRow from './TopRow';
import KeyRow from './KeyRow';
import CalendarRow from './CalendarRow';
import Calendar from './Calendar';
import PropTypes from 'prop-types';

import startOfWeek from 'date-fns/start_of_week';
import endOfWeek from 'date-fns/end_of_week';
import eachDay from 'date-fns/each_day';
import format from 'date-fns/format';
import addDays from 'date-fns/add_days';
import addMonths from 'date-fns/add_months';
import subMonths from 'date-fns/sub_months';
import addWeeks from 'date-fns/add_weeks';
import subWeeks from 'date-fns/sub_weeks';
import subDays from 'date-fns/sub_days';
import startOfYear from 'date-fns/start_of_year';
import startOfMonth from 'date-fns/start_of_month';
import endOfMonth from 'date-fns/end_of_month';
import setMonth from 'date-fns/set_month';

class CalendarWrapper extends Component {
    state = {
        view: 'Month',
        currentDate: new Date(),
        selectedDate: new Date(),
    };

    changeView = (event) => {
        this.setState({
            view: event.target.textContent,
        });
    };

    onDateClick = (day) => {
        this.setState({
            selectedDate: day,
        });
    };

    updateDate = (view, value) => {
        if (view === 'Month') {
            this.setState({
                currentDate: setMonth(new Date(this.state.currentDate), value),
            });
        } else if (view === 'Week') {
            this.getWeeksWithDates(value);
        } else if (view === 'Day') {
            this.setState({
                currentDate: new Date(value),
            });
        }
    };

    handleChange = (event) => {
        const index = event.nativeEvent.target.selectedIndex;
        const value = event.nativeEvent.target[index].getAttribute('time');
        const view = event.nativeEvent.target[index].getAttribute('view');
        this.updateDate(view, value);
    };

    removeDuplicates = (arr) => {
        let obj = {};
        let ret_arr = [];
        for (let i = 0; i < arr.length; i++) {
            obj[arr[i]] = true;
        }
        for (let key in obj) {
            ret_arr.push(key);
        }
        return ret_arr;
    };

    getWeekDays = () => {
        const dateFormat = 'dddd';
        const days = [];

        let startDate = startOfWeek(this.state.currentDate);

        for (let i = 0; i < 7; i++) {
            days.push(format(addDays(startDate, i), dateFormat));
        }
        return days;
    };

    getMonths = () => {
        const months = [];

        let startDate = startOfYear(this.state.currentDate);

        for (let i = 0; i < 12; i++) {
            months.push(addMonths(startDate, i));
        }
        return months;
    };

    getDays = () => {
        const start = startOfWeek(this.state.currentDate);
        const end = endOfWeek(this.state.currentDate);
        const allDays = eachDay(start, end);

        return allDays;
    };

    getStartWeeksForMonth = () => {
        const dates = [];

        const start = startOfMonth(this.state.currentDate);
        const end = endOfMonth(this.state.currentDate);

        const allDays = eachDay(start, end);

        allDays.forEach(day => {
            dates.push(`${startOfWeek(day)}`);
        });

        const fitlerStartWeeks = this.removeDuplicates(dates);

        return fitlerStartWeeks;
    };

    getEndWeeksForMonth = () => {
        const dates = [];

        const start = startOfMonth(this.state.currentDate);
        const end = endOfMonth(this.state.currentDate);

        const allDays = eachDay(start, end);

        allDays.forEach(day => {
            dates.push(`${endOfWeek(day)}`);
        });

        const filterEndWeeks = this.removeDuplicates(dates);

        return filterEndWeeks;
    };

    dropdownData = (view) => {
        if (view === 'Month') {
            const data = this.getMonths();
            return data;
        } else if (view === 'Day') {
            const data = this.getDays();
            return data;
        }
    };

    getWeeksWithDates = (current = this.state.currentDate) => {
        const start = startOfWeek(current);
        const end = endOfWeek(current);

        const dates = eachDay(start, end);

        return dates;
    };

    next = (view) => {
        if (view === 'Month') {
            this.setState({
                currentDate: addMonths(this.state.currentDate, 1),
            });
        } else if (view === 'Week') {
            this.setState({
                currentDate: addWeeks(this.state.currentDate, 1),
            });
        } else if (view === 'Day') {
            this.setState({
                currentDate: addDays(this.state.currentDate, 1),
            });
        }
    };

    prev = (view) => {
        if (view === 'Month') {
            this.setState({
                currentDate: subMonths(this.state.currentDate, 1),
            });
        } else if (view === 'Week') {
            this.setState({
                currentDate: subWeeks(this.state.currentDate, 1),
            });
        } else if (view === 'Day') {
            this.setState({
                currentDate: subDays(this.state.currentDate, 1),
            });
        }
    };

    render() {
        return (
            <div className="calendar p-3">
                <Container fluid>
                    <User />
                    <div className="calendar-main">
                        <TopRow
                            updateDate={this.updateDate}
                            startWeeks={this.getStartWeeksForMonth}
                            endWeeks={this.getEndWeeksForMonth}
                            next={this.next}
                            prev={this.prev}
                            view={this.state.view}
                            dropdownData={this.dropdownData}
                            changeView={this.changeView}
                            handleChange={this.handleChange}
                        />
                        <KeyRow />
                        <CalendarRow view={this.state.view}>
                            <Calendar
                                dateClick={this.onDateClick}
                                view={this.state.view}
                                selectedDate={this.state.selectedDate}
                                currentDate={this.state.currentDate}
                                weeksWithDates={this.getWeeksWithDates}
                                getWeekDays={this.getWeekDays}
                            />
                        </CalendarRow>
                    </div>
                </Container>
            </div>
        );
    }
}

CalendarWrapper.propTypes = {
    changeView: PropTypes.func,
    getWeekDays: PropTypes.func,
    getMonths: PropTypes.func,
    dropdownData: PropTypes.func,
    getWeeksWithDates: PropTypes.func,
    next: PropTypes.func,
    prev: PropTypes.func,
};

export default CalendarWrapper;
