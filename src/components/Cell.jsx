import React from 'react';

const Cell = ({ className, time }) => <div className={className}>{time}</div>;

export default Cell;
