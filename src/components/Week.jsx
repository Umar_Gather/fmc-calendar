import React from 'react';
import Cell from './Cell';

const Week = () => {
    function renderBlocks() {
        const max = 168;
        return Array.from({ length: max }, (_, i) => (
            <Cell key={i} className="row-item day" />
        ));
    }

    return <div className="grid-row week">{renderBlocks()}</div>;
};

export default Week;
