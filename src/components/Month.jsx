import React, { Component, Fragment } from 'react';
import dateFns from 'date-fns';
class Month extends Component {
    state = {};

    renderCells() {
        const { currentDate, selectedDate } = this.props;
        const monthStart = dateFns.startOfMonth(currentDate);
        const monthEnd = dateFns.endOfMonth(monthStart);
        const startDate = dateFns.startOfWeek(monthStart);
        const endDate = dateFns.endOfWeek(monthEnd);

        const dateFormat = 'D';
        const rows = [];

        let days = [];
        let day = startDate;
        let formattedDate = '';

        while (day <= endDate) {
            for (let i = 0; i < 7; i++) {
                formattedDate = dateFns.format(day, dateFormat);
                const cloneDay = day;
                days.push(
                    <div
                        className={`row-item day ${
                            !dateFns.isSameMonth(day, monthStart)
                                ? 'disabled'
                                : dateFns.isSameDay(day, selectedDate)
                                    ? 'selected'
                                    : ''
                        }`}
                        key={day}
                        onClick={() =>
                            this.props.dateClick(dateFns.parse(cloneDay))
                        }
                        time={day}
                    >
                        <span className="d-block">{formattedDate}</span>
                    </div>,
                );
                day = dateFns.addDays(day, 1);
            }
            rows.push(
                <div className="grid-row" key={day}>
                    {days}
                </div>,
            );
            days = [];
        }
        return rows;
    }

    render() {
        return <Fragment>{this.renderCells()}</Fragment>;
    }
}

export default Month;
