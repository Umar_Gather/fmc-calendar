const Bookings = [
    {
        time: '1PM',
        guests: 2,
        bookingName: 'Lol',
        refNo: 111222,
        status: 'Request',
    },
    {
        time: '10PM',
        guests: 10,
        bookingName: 'Lol',
        refNo: 111222,
        status: 'Request',
    },
    {
        time: '2PM',
        guests: 3,
        bookingName: 'Lol',
        refNo: 1231222,
        status: 'Cancelled',
    },
    {
        time: '3PM',
        guests: 1,
        bookingName: 'Lol',
        refNo: 1231231222,
        status: 'Booking',
    },
];

export default Bookings;
