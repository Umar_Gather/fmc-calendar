import React from 'react';

const Rows = ({ items }) => {
    return items.map((item) => (
        <tr className="trow">
            {Object.entries(item).map(([name, value]) => (
                <td className="tdata">{value}</td>
            ))}
        </tr>
    ));
};

const CancelTable = ({ items }) => (
    <div className="section cancel">
        <span className="heading-secondary d-block">Cancellation</span>
        <table className="table table-custom striped">
            <thead className="thead">
                <tr className="trow">
                    <th className="theading" scope="col">
                        Time
                    </th>
                    <th className="theading" scope="col">
                        Guests
                    </th>
                    <th className="theading" scope="col">
                        Booking Name
                    </th>
                    <th className="theading" scope="col">
                        Ref No.
                    </th>
                    <th className="theading" scope="col">
                        Status
                    </th>
                </tr>
            </thead>
            <tbody>
                <Rows items={items} />
            </tbody>
        </table>
    </div>
);

export default CancelTable;
