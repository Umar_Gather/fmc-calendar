import React from 'react';
import { Row, Col } from 'reactstrap';
import ArrowButtons from './ArrowButtons';
import SelectDropdown from './SelectDropdown'; // eslint-disable-line
import ViewsList from './ViewsList'; // eslint-disable-line

const TopRow = ({
    next,
    prev,
    view,
    currentDate,
    dropdownData,
    changeView,
    handleChange,
    updateDate,
    startWeeks,
    endWeeks
}) => (
    <div className="top-row p-1">
        <Row>
            <Col xs="1" sm="2">
                <ArrowButtons next={next} prev={prev} view={view} />
            </Col>
            <Col xs="8" sm="7">
                <SelectDropdown
                    updateDate={updateDate}
                    startWeeks={startWeeks}
                    endWeeks={endWeeks}
                    view={view}
                    currentDate={currentDate}
                    dropdownData={dropdownData}
                    handleChange={handleChange}
                />
            </Col>
            <Col xs="3" sm="3">
                <ViewsList changeView={changeView} />
            </Col>
        </Row>
    </div>
);

export default TopRow;
