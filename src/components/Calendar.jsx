import React, { Fragment } from 'react';
import GridHeading from './GridHeading';
import Grid from './Grid';

const Calendar = ({
    view,
    weeksWithDates,
    hoursOfDay,
    getWeekDays,
    dateClick,
    currentDate,
    selectedDate
}) => (
    <Fragment>
        <div className="grid">
            <GridHeading
                view={view}
                weeksWithDates={weeksWithDates}
                hoursOfDay={hoursOfDay}
                getWeekDays={getWeekDays}
            />
            <Grid
                dateClick={dateClick}
                view={view}
                currentDate={currentDate}
                selectedDate={selectedDate}
            />
        </div>
    </Fragment>
);

export default Calendar;
