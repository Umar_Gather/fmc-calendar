import React, { Component } from 'react';

class ViewsList extends Component {
    state = {
        views: ['Month', 'Week', 'Day'],
    };
    render() {
        return (
            <ul className="views">
                {this.state.views.map((view, i) => {
                    return (
                        <li
                            key={i}
                            className="view"
                            onClick={this.props.changeView}
                        >
                            {view}
                        </li>
                    );
                })}
            </ul>
        );
    }
}

export default ViewsList;
