import React from 'react';
import { Row, Col } from 'reactstrap';

const KeyRow = () => (
    <div className="key-row p-1">
        <Row>
            <Col xs="12">
                <ul className="key">
                    <li className="key-item">
                        <span className="circle request" />
                        Request
                    </li>
                    <li className="key-item">
                        <span className="circle booking" />
                        Booking
                    </li>
                    <li className="key-item">
                        <span className="circle cancellation" />
                        Cancellation
                    </li>
                </ul>
            </Col>
        </Row>
    </div>
);

export default KeyRow;
