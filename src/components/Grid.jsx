import React, { Fragment } from 'react';
import Month from './Month';
import Week from './Week';
import Day from './Day';

const Grid = ({ view, dateClick, currentDate, selectedDate }) => (
    <Fragment>
        {view === 'Month' && (
            <Month
                dateClick={dateClick}
                view={view}
                currentDate={currentDate}
                selectedDate={selectedDate}
            />
        )}
        {view === 'Week' && <Week view={view} />}
        {view === 'Day' && <Day view={view} currentDate={currentDate} />}
    </Fragment>
);

export default Grid;
