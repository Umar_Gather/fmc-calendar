import React, { Fragment } from 'react';
import PropTypes from 'prop-types';
import { Row, Col } from 'reactstrap';
import WeekColumn from './WeekColumn'; //eslint-disable-line

const CalendarRow = ({ view, children }) => (
    <div className="calendar-row pt-1 pb-4 px-4">
        <Row>
            {view === 'Week' ? (
                <Fragment>
                    <Col xs="2" className="pr-0">
                        <WeekColumn />
                    </Col>
                    <Col xs="10" className="pl-0">
                        {children}
                    </Col>
                </Fragment>
            ) : (
                <Col xs="12">{children}</Col>
            )}
        </Row>
    </div>
);

CalendarRow.propTypes = {
    view: PropTypes.string.isRequired,
};

export default CalendarRow;
